<?php

namespace Database\Seeders;

use App\Models\Attachment;
use App\Models\Comment;
use App\Models\Country;
use App\Models\Project;
use App\Models\Task;
use App\Models\Team;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user1 = User::factory()->create();
        $user2 = User::factory()->create();
        $project = Project::create(['name' => 'Some project']);
        DB::table('project_user')->insert([
            'project_id' => $project->id,
            'user_id' => $user1->id,
            'start_date' => now()->toDateString()
        ]);
    }
}
