<?php

namespace App\Http\Controllers;

use App\Models\Country;
use Illuminate\Support\Facades\DB;

class CountryController extends Controller
{
    public function index()
    {
        // TODO 17: test_countries_with_team_size
        // load the relationship average of team size
        $countries = Country::with('teams')
            ->selectRaw('avg(teams.size) as teams_avg_size')
            ->from('teams')
            ->get();

        return view('countries.index', compact('countries'));
    }
}
