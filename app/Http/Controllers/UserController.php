<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function index()
    {
        // TODO 19: test_filter_users
        $users = DB::table('users')
            ->join('project_user', 'users.id', '=', 'project_user.user_id')
            ->select('users.name', 'users.email')
            ->where('project_user.project_id','>',0)
            ->get();

        return view('users.index', compact('users'));
    }

    public function show($name)
    {
        $user = User::where('name', $name)->first();
        if (!$user) {
            return view('users.notfound');
        }

        return view('users.show', compact('user'));
    }

    public function showRelationship(User $user)
    {

        // TODO 14.2: test_show_users_comments
        // fix this by editing some code. Maybe there is too much?
        $user = User::with('comments')->find($user->id);
        return view('users.show', compact('user'));
    }
}
